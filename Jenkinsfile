pipeline {
  environment {
    REPOSITORY = 'rpassmore'
    IMAGE = 'jenkins-test'
    TAG = '0.0.1'
  }
  agent {
    kubernetes {
      defaultContainer 'jnlp'
      yamlFile 'jenkins-pod.yml'
    }
  }
  stages {
    stage('Checkout') {
      steps {
        git 'https://gitlab.com/rpassmore/jenkins-example.git'
      }
    }

    stage("Lint dockerfiles") {
      steps {
        container('hadolint') {
          sh "hadolint -f json Dockerfile | tee -a hadolint_lint.json"
        }
      }
      post {
        always {          
          recordIssues enabledForFailure: true,
            tools: [hadoLint(pattern: 'hadolint_lint.json')],
            qualityGates: [[threshold: 1, type: 'TOTAL']]
          script {
            //Work around for JENKINS-62269
            if (currentBuild.currentResult == 'FAILURE') {
              error('Lint: Warnings found pipeline failed.')
            }
          }
        }
      }
    }
    stage ("Building container image") {
      environment {
        PATH = "/busybox:$PATH"
        KANIKO_ARGS = "--use-new-run=true --snapshotMode=redo --no-push=true"
      }
      steps {
        container(name: 'kaniko', shell: '/busybox/sh') {
          ansiColor('xterm') {
            sh '''#!/busybox/sh
              /kaniko/executor --context `pwd` ${KANIKO_ARGS} --destination=${REPOSITORY}/${IMAGE}:${TAG}
            '''
          }
        }
      }
    }
    stage ("Scan container image") {
     steps {
        container("trivy") {
          sh  '''
            trivy --no-progress --exit-code=1 -o trivy.json ${REPOSITORY}/${IMAGE}:${TAG}
          '''
        }
      }
      post {
        always {          
          recordIssues enabledForFailure: true,
            tools: [trivy(pattern: 'trivy.json')],
            qualityGates: [[threshold: 1, type: 'TOTAL']]
          script {
            //Work around for JENKINS-62269
            if (currentBuild.currentResult == 'FAILURE') {
              error('Scan: Vulns found pipeline failed.')
            }
          }
        }
      }
    }

    stage ("Push container image") {
      environment {
        PATH        = "/busybox:$PATH"
        KANIKO_ARGS = "--use-new-run=true --snapshotMode=redo"
      }
      steps {
        container(name: 'kaniko', shell: '/busybox/sh') {
          ansiColor('xterm') {
            sh '''#!/busybox/sh
              #for n in \$(find . -type f -name Dockerfile); do if ! hadolint \$n; then ec=1;
              /kaniko/executor --context `pwd` ${KANIKO_ARGS} --destination=${REPOSITORY}/${IMAGE}:${TAG} --destination=${REPOSITORY}/${IMAGE}:latest
            '''
          }
        }
      }
    }
  }
}
